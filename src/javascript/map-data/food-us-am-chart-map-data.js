function FoodUSAMChartMapData (attrs) {
  this._attrs = attrs
  this.searchTerm = this.searchTerm()
  this.data = this.data()
}

FoodUSAMChartMapData.prototype.searchTerm = function() {
  return this._attrs.searchTerm
}

FoodUSAMChartMapData.prototype.data = function() {
  var key
    , data
    , newData
    , value

  data = this._attrs.data
  newData = []

  for (key in data) {
    value = data[key];
    if(data.hasOwnProperty(key)){
      newData.push({id: "US-" + key
                   , value: value})
    }
  }

  return newData
}

module.exports = FoodUSAMChartMapData