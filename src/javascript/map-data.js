var Qajax = require('qajax')
  , Q = require('q')
  , FoodUSAMChartMapData = require('./map-data/food-us-am-chart-map-data')

function fetchMapData (id) {
  return Qajax('http://food-us-api.herokuapp.com/maps/1')
         .then(Qajax.toJSON)
         .then(prepareMapDataForAMCharts)
}

function prepareMapDataForAMCharts (data) {
  return Q.fcall(function() {
           return new FoodUSAMChartMapData(data)
         })
}

module.exports = fetchMapData