module.exports = function(data) {
  var theme = require('./map-theme')
  var map = AmCharts.makeChart("chartdiv", {
    type: "map"

  , "theme": "custom"
  , pathToImages: "http://www.amcharts.com/lib/3/images/"

  , colorSteps: 20
  , balloonLabelFunction: function(a) {
      return a.value + ' menu items';
    }

  , dataProvider: {
    map: "usaLow"
  , areas: data.data
  }

  , valueLegend: {
    right: 10
  , minValue: "0 items"
  , maxValue: "100 items"
  }
  })
}