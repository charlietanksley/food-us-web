var router = require('page')
var routes = require('./routes')

router('/', routes.welcome)
router('/maps/:id', routes.maps)

module.exports = router
