AmCharts.themes.custom = {
  AreasSettings: {
    alpha: 0.8,
    color: "#c2a5fc",
    colorSolid: "#4407BD",
    unlistedAreasAlpha: 0.4,
    unlistedAreasColor: "#000000",
    outlineColor: "#FFFFFF",
    outlineAlpha: 0.5,
    outlineThickness: 0.5,
    rollOverColor: "#5609ee",
    rollOverOutlineColor: "#FFFFFF",
    selectedOutlineColor: "#FFFFFF",
    selectedColor: "#BD0784",
    unlistedAreasOutlineColor: "#FFFFFF",
    unlistedAreasOutlineAlpha: 0.5
  },

  LinesSettings: {
    color: "#000000",
    alpha: 0.8
  },

  ZoomControl: {
    panControlEnabled: false,
    zoomControlEnabled: false
  },
}

module.exports = AmCharts.themes.custom