module.exports = React.createClass({
  getInitialState: function() {
    return {
      searchTerm: ''
    }
  }

, handleChange: function(event) {
    this.setState({
      searchTerm: event.target.value
    })
  }

, handleSubmit: function(event) {
    var performSearch = require('../search/perform')
    event.preventDefault()

    performSearch.run(this.state)
}

, render: function() {
    var instructions
      , finePrint
      , form
      , searchTermInput
      , submitInput
      , title

    finePrint = React.DOM.p(
      {className: 'fine-print'},
      'Where "popularity" is determined by number of menu items found in the state via ',
      React.DOM.a({href: 'https://dev.locu.com'}, 'the locu api'),
      '.')

    title = React.DOM.h2({className: 'new-map-header'}, 'Create a new map')

    instructions = React.DOM.div(
      {className: 'new-map-instructions'}
    , [React.DOM.div(null, "To create a popularity map* for your favorite food, just enter it in the form and hit submit!")
      , finePrint
      ])

    searchTermInput = React.DOM.input(
      { className: 'search-term-input'
      , type:"text"
      , placeholder: 'name here'
      , onChange: this.handleChange
      , value: this.state.searchTerm })

    submitInput = React.DOM.p(
      {className: 'new-map-submit'}
    , React.DOM.button(
      { type:"submit"}
    , 'Create My Map'))

    form = React.DOM.form(
      {className: 'new-map-form'
      , onSubmit: this.handleSubmit}
    , [searchTermInput
      , submitInput
      ])

    return React.DOM.div(
      {className: 'new-map-container'},
      [instructions
      , form])
  }
})();
