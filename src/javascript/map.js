module.exports = function(data) {
  return React.DOM.section(
    {className: 'map-holder'}
  , [React.DOM.h2({className: 'section-heading'}, data.searchTerm)
    , React.DOM.div({id:"chartdiv"})
    ]
  )
}