var compass      = require('gulp-compass');
var gulp         = require('gulp');
var livereload   = require('gulp-livereload');
var notify       = require('gulp-notify');
var handleErrors = require('../util/handleErrors');

gulp.task('compass', function() {
        return gulp.src('./src/scss/**/*.scss')
                .pipe(compass({
                        config_file: 'compass.rb',
                        css: 'build',
                        sass: 'src/scss'
                }))
                .on('error', handleErrors)
                .pipe(livereload());
});
