function showPage(component) {
  React.renderComponent(
    component,
    document.getElementById('main')
  )
}

function welcomeRoute() {
  var form

  form = require('./new-map/form')
  showPage(form)
}

function mapsShowRoute(context) {
  var id = context.params.id
  var mapData = require('./map-data')
    , map = require('./map')
    , insertMap = require('./insert-map')

  mapData(id)
  .then(function(data) {
    showPage(map(data))
    return data
  })
  .then(function(data) {
    insertMap(data)
  })
}

var routes = {
  welcome: welcomeRoute
, maps: mapsShowRoute
}

module.exports = routes
